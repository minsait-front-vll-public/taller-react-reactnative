# TALLER DE REACT & REACT NATIVE
### Impartido por: David Fernández y Ricardo Vega, arquitectos Front en Minsait.


### Fecha: 16 de mayo de 2018, a las 17h
### Lugar: Salón de Actos (planta baja) del Edificio Parque Científico de la UVA (CTTA). Campus Miguel Delibes - Valladolid
[Ver Ubicación](https://goo.gl/maps/BXygDABTAqE2)

## Autores:

- **David Fernández Perrino**: [Linkedin](https://www.linkedin.com/in/davidfernandezperrino/)
- **Ricardo Vega Alonso**: [Linkedin](https://www.linkedin.com/in/ricveal/) [Twitter](https://twitter.com/ricveal) [Blog](https://ricveal.com/)

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2980.608688321469!2d-4.706350684505144!3d41.664195986751956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd47129e2c27d3ab%3A0x3b2bf28ff45590d0!2sEDIFICIO+CTTA!5e0!3m2!1ses!2ses!4v1525426436577" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

Preinscripción y contacto: [dfperrino@minsait.com](mailto:dfperrino@minsait.com)

![](https://media.licdn.com/media-proxy/ext?w=3579&h=4760&f=n&hash=Y%2BTzPJ6DNd5a7fRtcPT6UWIyGGc%3D&ora=1%2CaFBCTXdkRmpGL2lvQUFBPQ%2CxAVta5g-0R6jnhodx1Ey9KGTqAGj6E5DQJHUA3L0CHH05IbfPWjpLM6Nf7qooEAXfy8JjQBheO61ETDnRo69L47rLIokiZa0cpj5aRUPbhU4hGUB_N88)