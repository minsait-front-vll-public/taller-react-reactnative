# Requisitos

Para aprovechar al máximo el taller, os recomendamos que traigáis el siguiente equipo / software instalado, ya que no podremos abordar estas tareas en el mismo:

- Ordenador portátil Mac o PC. Podremos utilizar de forma similiar Windows, Linux o MacOSX.
- NodeJS instalado. Recomendamos la última versión estable (LTS), actualmente la [8.11.1](https://nodejs.org/es/download/) 
- IDE de desarrollo. Nosotros recomendamos [Visual Studio Code](https://code.visualstudio.com/), pero podéis utilizar el que más os guste.
- Herramientas CLI de react y react-native instalados. Para ello, seguid los pasos que se comentan un poco más adelante.
- SDK Android. También tenéis unos pasos para hacerlo un poco más adelante.
- (Opcional) Xcode para iOS (sólo en Mac OS X).

## Instalación herramientas CLI de react & react-native

Estas herramientas son proporcionadas por los autores de React & React Native para tener un punto de inicio rápido de aplicaciones de este tipo. Una vez tengamos nodeJS instalado, abriremos una consola de comandos en nuestro equipo y ejecutaremos el siguiente comando:

`npm install -g create-react-app create-react-native-app`

De esta forma tendremos ambos paquetes instalados, tanto el CLI para aplicaciones React, como el de aplicaciones React Native.

## Instalación de Android SDK

Android es la única plataforma que vamos a poder ejecutar en todos los equipos (Windows, Linux y/o MacOSX) ya que para "simular" o debuggear aplicaciones para IOS creadas con React Native, necesitamos forzosamente MacOSX. Por ello, veamos como preparar nuestro equipo para simular aplicaciones Android. Básicamente, tenemos dos opciones:

1. Instalar Android Studio (recomendada), que incluye un IDE para desarrollar aplicaciones Android, además del SDK de Android. Podéis descargarlo de [aquí](https://developer.android.com/studio/?hl=es-419#downloads).
2. Si no queréis instalar Android Studio, en realidad sólo se necesita el SDK de Android, que incluye también el emulador que utilizaremos, eso sí, tened en cuenta que su uso es por línea de comandos. Podéis descargarlo de [aquí](https://developer.android.com/studio/?hl=es-419#command-tools).

Tanto si lo hacéis de una manera o de otra, tendréis que instalar una versión de android. Con el Android Studio os guiará por un Asistente, que utilizando las opciones por defecto os proporcionará un entorno estable y suficiente para los propósitos del taller.
Si elegís la opción "dura", os recomendamos instalar la versión 27 de android que es la que instala por defecto Android Studio, así como los build-tools, etc de la misma versión, para intentar unificar. 

## Instalación de XCode (sólo MacOSX)

Si queréis instalar XCode para simulación y debuggeo de aplicaciones IOS, podéis conseguirlo (gratuitamente) en la App Store.

## Dudas / Problemas con la instalación

No dudéis en contactar con nosotros, en [dfperrino@minsait.com](mailto:dfperrino@minsait.com) o en [rvalonso@minsait.com](mailto:rvalonso@minsait.com)