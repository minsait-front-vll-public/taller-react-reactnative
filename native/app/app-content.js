import React, { Component } from 'react';
import { Content, Text, List, ListItem, Thumbnail, Body, Spinner } from 'native-base';
import { StyleSheet } from 'react-native';
import { /* database, */ auth } from './config/firebase';

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
  },
  texto: {
    textAlign: 'center',
  },
});

class MainContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locales: [],
      isLoading: true,
    };
  }

  componentWillMount() {
    /**
     * Dos maneras diferentes de hacer lo mismo:
     * 1) Llamada a un servicio REST con fetch
     * 2) Usando la API de firebase
     */
    auth.currentUser.getIdToken(true)
      .then((idToken) => {
        fetch(`https://taller-react.firebaseio.com/locales.json?auth=${idToken}`)
          .then(response => response.json())
          .then((res) => {
            this.setState({ isLoading: false, locales: res });
          });
      })
      .catch((error) => {
        console.error(error);
      });
    /* database.ref('/locales').once('value')
      .then((snapshot) => {
        this.setState({ isLoading: false, locales: snapshot.val() });
      })
      .catch((err) => { console.error(err); }); */
  }

  render() {
    return (
      <Content padder>
        <Text style={styles.texto}>
          Taller React Native
        </Text>
        {this.state.isLoading &&
          <Spinner size={100} style={styles.spinner} color="red" />
        }
        {!this.state.isLoading &&
          <List>
            {this.state.locales.map(local => (
              <ListItem key={local.nombre}>
                <Thumbnail square size={80} source={{ uri: local.img }} />
                <Body>
                  <Text>{local.nombre}</Text>
                  <Text note>{local.direccion}</Text>
                </Body>
              </ListItem>
            ))}
          </List>
        }
      </Content>
    );
  }
}

export default MainContent;
