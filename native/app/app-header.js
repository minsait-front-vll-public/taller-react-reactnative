
import React from 'react';
import { Header, Title, Button, Icon, Left, Right, Body } from 'native-base';

/**
 * Cuando no necesitamos usar los métodos del ciclo de vida del componente, etc
 * y el componente es sencillamente un resultado de utilizar sus properties, lo
 * podemos escribir como una función y no como una clase
 */
export default () => (
  <Header>
    <Left>
      <Button transparent>
        <Icon name="arrow-back" />
      </Button>
    </Left>
    <Body>
      <Title>Header</Title>
    </Body>
    <Right>
      <Button transparent>
        <Icon name="menu" />
      </Button>
    </Right>
  </Header>
);
