import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, StyleSheet } from 'react-native';
import { auth } from './config/firebase';

const image = require('./assets/splash.jpg');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    left: 0,
  },
  image: {
    flex: 1,
  },
});

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auth: false,
    };
  }

  componentWillMount() {
    /**
     * Autenticamos a un usuario que hayamos dado previamente permiso.
     * Evidentemente, lo suyo sería hacer un login
     */
    auth.signInWithEmailAndPassword('taller@minsait.com', 'minsait')
      .then(() => {
        this.setState({ auth: true });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  componentDidMount() {
    /**
     * Mientras no se haya autenticado, no navegamos a la pantalla principal
     */
    const interval = setInterval(() => {
      if (this.state.auth) {
        clearInterval(interval);
        this.props.navigation.navigate('Home');
      }
    }, 4000);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={image} resizeMode="cover" />
      </View>
    );
  }
}

Splash.propTypes = {
  navigation: PropTypes.object.isRequired,
};

Splash.navigationOptions = {
  header: null,
};

export default Splash;
