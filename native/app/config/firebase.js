import firebase from 'firebase';
import { config } from './constants';

firebase.initializeApp(config);

export const database = firebase.database();
export const auth = firebase.auth();
