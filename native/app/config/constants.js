import ENV from './ENV';

/* eslint-disable */
export const config = {
  apiKey: process.env.FIREBASE_APIKEY || ENV.FIREBASE_APIKEY,
  authDomain: 'taller-react.firebaseapp.com',
  databaseURL: 'https://taller-react.firebaseio.com',
  projectId: 'taller-react',
  storageBucket: 'taller-react.appspot.com',
  messagingSenderId: process.env.FIREBASE_SENDER || ENV.FIREBASE_SENDER,
};
