
import React, { Component } from 'react';
import { Container, Spinner } from 'native-base';
import { StyleSheet } from 'react-native';
import AppHeader from './app-header';
import AppContent from './app-content';

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  /**
   * Otra alternativa en vez de dar color a la barra de android en app.json es dar un margen
   * a la "aplicación" del tamaño del status bar, lo cual se obtiene de la siguiente variable
   * header: {
   *   marginTop: Expo.Constants.statusBarHeight,
   * },
   */
});

/**
 * Componente base de la aplicación
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
    };
  }

  componentWillMount() {
    /**
     * Para cargar las tipografías de Expo, recomiendan cargarlas asíncronamente y no renderizar
     * la aplicación hasta que no se hayan cargado dichas fuentes
     */
    Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
    }).then(() => {
      this.setState({ isLoaded: true });
    });
  }

  /**
   * Render de la aplicación. Si se han cargado las tipografías, se renderiza,
   * si no, se muestra un spinner (cargador)
   */
  render() {
    if (this.state.isLoaded) {
      return (
        <Container>
          <AppHeader />
          <AppContent />
        </Container>
      );
    }
    return <Spinner size={100} style={styles.spinner} color="red" />;
  }
}

App.navigationOptions = {
  header: null,
};

export default App;
