
import { StackNavigator } from 'react-navigation';
import HomeScreen from './app/home';
import SplashScreen from './app/splash';

const App = StackNavigator({
  Splash: { screen: SplashScreen },
  Home: { screen: HomeScreen },
});
export default App;
