import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { firebase } from './firebase';
// Los diferentes componentes que conforman la aplicación
import Navigation from './components/Navigation';
import LandingPage from './components/Landing';
import SignUpPage from './components/SignUp';
import SignInPage from './components/SignIn';
import HomePage from './components/Home';
// Listado de las rutas
import * as routes from './routes';
// Estilo de la aplicación
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      authUser: null,
    };
  }

  /**
   * Método perteneciente al ciclo de vida de un componente de React que se ejecuta automáticamente
   * justo después de que la aplicación haya sido renderizada. Se vuelve a ejecutar cada vez
   * que el componente se vuelve a renderizar.
   *
   * En este caso concreto, maneja la autenticación.
   *
   * @memberof App
   */
  componentDidMount() {
    firebase.auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        this.setState(() => ({ authUser }));
      } else {
        this.setState(() => ({ authUser: null }));
      }
    });
  }
  render() {
    // Todo el manejo de rutas de la aplicación.
    return (
      <Router>
        <div>
          <Navigation authUser={this.state.authUser} />

          <Route
            exact
            path={routes.LANDING}
            component={() => <LandingPage />}
          />
          <Route
            exact
            path={routes.HOME}
            component={() => <HomePage />}
          />
          <Route
            exact
            path={routes.SIGN_UP}
            component={() => <SignUpPage />}
          />
          <Route
            exact
            path={routes.SIGN_IN}
            component={() => <SignInPage />}
          />
        </div>
      </Router>
    );
  }
}


export default App;
