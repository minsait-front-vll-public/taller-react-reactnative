/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';
import 'element-theme-default/lib/index.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// En el elemento del DOM con id 'root', renderiza la aplicación.
ReactDOM.render(<App />, document.getElementById('root'));
// Registro del service worker. No tratado en este taller.
registerServiceWorker();
