import * as auth from './auth';
import * as firebase from './firebase';

// Unificamos en un punto común los diferentes elementos dependientes de Firebase.

export {
  auth,
  firebase,
};
