import * as firebase from 'firebase';

const config = require('../secrets.json');

// Inicializamos la librería de Firebase

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();

export { auth };
