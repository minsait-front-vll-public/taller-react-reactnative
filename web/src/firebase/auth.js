import { auth } from './firebase';

// Registro
export const doCreateUserWithEmailAndPassword = (email, password) =>
  auth.createUserWithEmailAndPassword(email, password);

// Login
export const doSignInWithEmailAndPassword = (email, password) =>
  auth.signInWithEmailAndPassword(email, password);

// Logout
export const doSignOut = () =>
  auth.signOut();

/*
 * Podemos tener más elementos gracias a la API de Firebase, aunque en el ejemplo concreto
 * de esta aplicación, no usaremos los siguientes.
*/

// Restablecimiento de la contraseña.
export const doPasswordReset = email =>
  auth.sendPasswordResetEmail(email);

// Cambio de contraseña.
export const doPasswordUpdate = password =>
  auth.currentUser.updatePassword(password);
