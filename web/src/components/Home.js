import React from 'react';
import Icon from 'react-icons-kit';
import { users } from 'react-icons-kit/icomoon/users';
import { smile } from 'react-icons-kit/icomoon/smile';
import { coinEuro } from 'react-icons-kit/icomoon/coinEuro';
import { Layout, Card, Rate } from 'element-react';
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

// Datos mock que representan el número de visitantes en cada hora.
const data = [
  { name: '08', visitors: 1 },
  { name: '09', visitors: 4 },
  { name: '10', visitors: 4 },
  { name: '11', visitors: 11 },
  { name: '12', visitors: 7 },
  { name: '13', visitors: 3 },
  { name: '14', visitors: 7 },
  { name: '15', visitors: 4 },
  { name: '16', visitors: 0 },
  { name: '17', visitors: 1 },
  { name: '18', visitors: 2 },
  { name: '19', visitors: 3 },
  { name: '20', visitors: 1 },
  { name: '21', visitors: 1 },
];

/**
 * Componente que renderiza la página principal.
 *
 * Todos los datos mostrados son mock ya que está página simplemente pretende ser ilustrativa
 * de las capacidades de React para hacer interfaces web.
 *
 */
const HomePage = () =>
  (
    <div className="home">
      <div className="rating">
        <span>Rating:</span>
        <Rate colors={['#FF4949', '#F7BA2A', '#13CE66']} disabled value={3.9} showText />
      </div>
      <Layout.Row gutter="20">
        <Layout.Col span="8">
          <Card className="box-card">
            <span>57 visitors </span><Icon icon={users} />
          </Card>
        </Layout.Col>
        <Layout.Col span="8">
          <Card className="box-card">
            <span>38 likes </span><Icon icon={smile} />
          </Card>
        </Layout.Col>
        <Layout.Col span="8">
          <Card className="box-card">
            <span>228,59 </span><Icon icon={coinEuro} />
          </Card>
        </Layout.Col>
      </Layout.Row>
      <ResponsiveContainer width="100%" aspect={21 / 9}>
        <BarChart className="hour-chart" data={data}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="visitors" fill="#13CE66" />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );

export default HomePage;
