import React from 'react';
import { Button } from 'element-react';
import { withRouter } from 'react-router-dom';

import { auth } from '../firebase';
import * as routes from '../routes';

/**
 * Devuelve el botón de Logout que se representa en la cabecera de la aplicación
 * cuando el usuario está autenticado.
 *
 */
const SignOutButton = ({ history }) =>
  <Button type="primary" onClick={() => auth.doSignOut().then(() => history.push(routes.LANDING))} icon="close">Sign Out</Button>;

export default withRouter(SignOutButton);
