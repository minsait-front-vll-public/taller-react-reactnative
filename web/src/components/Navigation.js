import React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'element-react';

import SignOutButton from './SignOut';

import * as routes from '../routes';

// Nombre de la aplicación.
const appName = 'Taller React';

/**
 * Componente Navegación que discierne entre si el usuario está o no autenticado para mostrar
 * qué componente que renderice la cabecera.
 *
 * @param {any} { authUser }
 */
const Navigation = ({ authUser }) =>
  (
    <div>
      { authUser
        ? <NavigationAuth />
        : <NavigationNonAuth />
    }
    </div>
  );

/**
 * Barra superior de navegación en el caso de que el usuario esté autenticado.
 *
 * Cuenta con un texto que sirve de logo del proyecto (cuyo click envía a la página de Landing)
 * y un botón para des-loguearse de la aplicación.
 *
 */
const NavigationAuth = () =>
  (
    <Menu theme="dark" defaultActive="1" className="el-menu-demo" mode="horizontal">
      <Link to={routes.LANDING}><Menu.Item className="app-name" index="1">{appName}</Menu.Item></Link>
      <Menu.Item className="logout" index="2"><SignOutButton /></Menu.Item>
    </Menu>
  );

/**
 * Barra superior de navegación cuando el usuario no está autenticado.
 *
 * Cuenta con un texto que sirve de logo del proyecto (cuyo click envía a la página de Landing)
 *
 */
const NavigationNonAuth = () =>
  (
    <Menu theme="dark" defaultActive="1" className="el-menu-demo" mode="horizontal">
      <Link to={routes.LANDING}><Menu.Item className="app-name" index="1">{appName}</Menu.Item></Link>
    </Menu>
  );

export default Navigation;
