import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Form, Input, Button } from 'element-react';
import { auth } from '../firebase';
import * as routes from '../routes';

/**
 * Página de registro de un nuevo usuario.
 *
 * @param {any} { history }
 */
const SignUpPage = ({ history }) =>
  (
    <div>
      <SignUpForm history={history} />
    </div>
  );

// Estado inicial.
const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

/**
 * Formulario de registro.
 *
 * @class SignUpForm
 * @extends {Component}
 */
class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = { ...INITIAL_STATE };
  }

  /**
   * Método para actualizar el estado del componente cuando el usuario introduce
   * datos en el formulario.
   *
   * @param {any} key
   * @param {any} value
   * @memberof SignUpForm
   */
  onChange(key, value) {
    this.setState({ [key]: value });
  }

  /**
   * Método que maneja el envío del formulario.
   *
   * @param {any} event
   * @memberof SignUpForm
   */
  onSubmit(event) {
    const {
      email,
      passwordOne,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.LANDING);
      })
      .catch((error) => {
        this.onChange('error', error);
      });
    event.preventDefault();
  }

  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    // Validaciones básicas en cliente.
    const isInvalid =
    passwordOne !== passwordTwo ||
    passwordOne === '' ||
    email === '' ||
    username === '';

    return (
      <Form className="signup-form" model={this.state.form} labelWidth="100">
        <h1>SignUp</h1>
        <Form.Item label="Username">
          <Input value={username} onChange={value => this.onChange('username', value)} />
        </Form.Item>
        <Form.Item label="Email Address">
          <Input value={email} onChange={value => this.onChange('email', value)} />
        </Form.Item>
        <Form.Item label="Password">
          <Input value={passwordOne} type="password" onChange={value => this.onChange('passwordOne', value)} />
        </Form.Item>
        <Form.Item label="Confirm Password">
          <Input value={passwordTwo} type="password" onChange={value => this.onChange('passwordTwo', value)} />
        </Form.Item>
        <Button type="primary" disabled={isInvalid} onClick={this.onSubmit}>Sign Up</Button>

        { error && <p>{error.message}</p> }
      </Form>
    );
  }
}

const SignUpLink = () =>
  (
    <p>
      {'Don\'t have an account? '}
      <Link to={routes.SIGN_UP}>Sign Up</Link>
    </p>
  );

// Podemos exportar algún elemento por defecto.
export default withRouter(SignUpPage);

// y tener más elementos exportables dentro del mismo fichero.
export {
  SignUpForm,
  SignUpLink,
};
