import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Form, Input, Button } from 'element-react';
import { SignUpLink } from './SignUp';

import { auth } from '../firebase';
import * as routes from '../routes';

/**
 * Página encarga del Login.
 *
 * @param {any} { history }
 */
const SignInPage = ({ history }) =>
  (
    <div className="login">
      <div className="login login-form">
        <h1>SignIn</h1>
        <SignInForm history={history} />
        <SignUpLink />
      </div>
    </div>
  );

// Estado inicial y por defecto del formulario.
const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

/**
 * Formulario de Login
 *
 * @class SignInForm
 * @extends {Component}
 */
class SignInForm extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = { ...INITIAL_STATE };
  }

  /**
   * Método que se encarga de actualizar el estado interno del componente al producirse un
   * cambio en el formulario
   *
   * @param {any} key
   * @param {any} value
   * @memberof SignInForm
   */
  onChange(key, value) {
    this.setState({
      [key]: value,
    });
  }

  /**
   * Método que maneja la lógica de envío del formulario.
   *
   * Lanza el proceso de login contra Firebase.
   *   - En el caso de que sea exitoso, devuelve el estado
   *     al estado inicial y redirige la aplicación al Home.
   *   - En el caso de que no sea exitoso, actualiza el estado fijando una variable
   *     error con el error devuelto por Firebase.
   *
   *
   * @param {any} event
   * @memberof SignInForm
   */
  onSubmit(event) {
    const {
      email,
      password,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.HOME);
      })
      .catch(error => this.onChange('error', error));

    event.preventDefault();
  }

  render() {
    // Forma habitual de disponer de variables del estado de una forma más comoda.
    const {
      email,
      password,
      error,
    } = this.state;

    // Podemos definir variables dentro del render que sean usadas en mismo. Por ejemplo, en
    // caso, comprobamos que tanto email como password tengan contenido para, en caso contrario,
    // deshabilitar el botón de Login.
    const isInvalid =
      password === '' ||
      email === '';

    return (
      <Form model={this.state.form} labelWidth="100">
        <Form.Item label="Email Address">
          <Input value={email} type="text" onChange={value => this.onChange('email', value)} />
        </Form.Item>
        <Form.Item label="Password">
          <Input value={password} type="password" onChange={value => this.onChange('password', value)} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" disabled={isInvalid} onClick={this.onSubmit}>Login</Button>
        </Form.Item>
        { error && <p className="error">{error.message}</p> }
      </Form>
    );
  }
}

export default withRouter(SignInPage);

export { SignInForm };
