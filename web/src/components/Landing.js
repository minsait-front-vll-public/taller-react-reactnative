import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Button } from 'element-react';
import * as routes from '../routes';

/**
 * Página simple que el usuario ve al entrar en la aplicación y desde donde accede
 * a la página de Login.
 *
 */
const LandingPage = () =>
  (
    <div className="landing">
      <Layout.Row>
        <h1>Tu buscador de locales en Valladolid</h1>
      </Layout.Row>
      <Layout.Row>
        <Button type="primary"><Link to={routes.SIGN_IN}>Login</Link></Button>
      </Layout.Row>
    </div>
  );

export default LandingPage;
