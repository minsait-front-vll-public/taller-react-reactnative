Este proyecto ha sido creado a través de la herramienta [Create React App](https://github.com/facebookincubator/create-react-app).

Dicha herramienta provee de una estructura inicial de proyecto con [ReactJS](https://reactjs.org/) que puede ser ampliada para lo cual incluyen una guía con algunas de las tareas más comunes. La última versión de esta guía puedes encontrarla [aquí](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

El objetivo de este proyecto es servir de demostración de lo rápido que se puede montar una aplicación empleando ReactJS.

A mayores, incluye Firebase como "backend" (y su integración con él).

## Instalación proyecto

> Por favor, antes de continuar, asegúrate de cumplir los [requisitos](../requisitos.md) de este taller.

Para instalar este proyecto, debes abrir una consola de comandos, situarte en él y ejecutar:

```
npm install
```

Este proyecto hace uso de Firebase por lo que será necesario indicar lso datos de configuración al mismo. Para ello,
debes crear el fichero `secrets.json` en el directorio `src` con el siguiente contenido:

```javascript
{
    "apiKey": "API_KEY",
    "authDomain": "URL",
    "databaseURL": "URL_DATABASE",
    "projectId": "PROJECT_ID",
    "storageBucket": "STORAGE_URL",
    "messagingSenderId": "SENDER_ID"
}
```

Dicha información, es visible en la página de Firebase desde el panel de bienvenida del proyecto, accediendo a "Agregar Firebase a tu app web".

## Scripts disponibles

En este proyecto puedes ejecutar:

### `npm start`

Ejecuta la aplicación en "modo desarrollo". Al ejecutarse, compilará el proyecto y abrirá el navegador por defecto en la dirección
[http://localhost:3000](http://localhost:3000).

La página se recargará automáticamente con cada edición que hagas ya que cuenta con la funcionalidad Hot Reloading.
Además, podrás ver cualquier error de "linting" en la consola de tu navegador.

### `npm test`

Lanzaría el ejecutor de test en modo interactivo, escuchando cambios.
Esta parte no se trata en este taller.

### `npm run build`

Construiría la aplicación para producción en la carpeta `build`.

Empaqueta React en modo producción y optimiza la construcción para obtener el mejor rendimiento.

El resultante es minificado para reducir su tamaño y ofuscar el código.

### `npm run eject`

**Ojo: Esta operación es irreversible. Una vez desacoplas, no hay vuelta atrás.**

La operación de desacople, copia todos los ficheros de configuración y dependencias de desarrollo, eliminando la abstracción que provee el paquete
'react-scripts' pero ampliando las capacidades de personalización de la aplicación.

### `npm run lint`

El proceso de "linting" es una comprobación estática de código en búsqueda de algunos de los errores más comunes, definidos en reglas. La herramienta que subyace por debajo es [eslint](https://eslint.org/) y el juego de reglas que estamos empleado es el definido en la guía de estilo de [AirBnb](https://github.com/airbnb/javascript).

### `npm run lint:fix`

Algunas de las reglas de "linting" pueden ser corregidas automáticamente por la herramienta. Para ello puedes ejecutar esta opción.

## Más información

El proyecto [Create React App](https://github.com/facebookincubator/create-react-app) cuenta con una muy buena documentación.
Puedes leer en su [README](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md)
sobre gran cantidad de propiedades, configuraciones y funcionalidades que es posible añadir a este proyecto.